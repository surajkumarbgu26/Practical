﻿//Practical-1. Program using namespace and comments print Hello.
/*
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Practical
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Output:
            Console.WriteLine("Hello");
        }
    }
}
*/

//OUTPUT: Hello

//----------------------------------------------------------------------

//Practical-2. Write program using conditional statements and loops:
//To test for prime numbers.
/*
using System;
public class Practical
{
    public static void Main(String[] args)
    {
        int num, counter;
        Console.Write("Enter the number to check prime or not: ");
        num = int.Parse(Console.ReadLine());
        for (counter = 2; counter <= num / 2; counter++) {
            if (num % counter == 0)
                break;
            }
            if (num == 1)
                Console.WriteLine(num + " is neither prime nor composite");
            else if (counter < (num / 2))
                Console.WriteLine(num + " is not a prime number");
            else
            {
                Console.WriteLine(num + " is prime number");
            }
        }
    }
*/

/*
OUTPUT:
Enter the number to check prime or nor: 1
1 is neither prime nor composite

OR,

Enter the number to check prime or nor: 11
11 is prime number
 */

//----------------------------------------------------------------------

//Practical-3: Write a Console application that places double Quotation marks around 
//each word in a string.

/*
  using System;
    public class Practical
    {
        static void Main(String[] args)
        {
            String str1;
            Console.Write("Enter the first String: ");
            str1 = Console.ReadLine();
            string[] words = str1.Split(' ');
            for (int i = 0; i < words.Length; i++)
            {
                Console.Write(" \" " + words[i] + " \" ");
            }
        }
    }

*/

// " hello "  " i "  " am "  " suraj "

//---------------------------------------------------------------------


//Practical-4: WriteOnlyArrayAttribute a console application that obtain four int values
//from the user and displays the producr

/*
using System;
public class Practical
{
    static void Main(String[] args)
    {
        int a, b, c, d;

        Console.Write("Enter the first number: ");
         a = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter the second number: ");
         b = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter the third number: ");
         c = Convert.ToInt32(Console.ReadLine());

        Console.Write("Enter the fourth numbr: ");
         d = Convert.ToInt32(Console.ReadLine());

        int e = a * b * c * d;
        Console.WriteLine("product is: " + e);
    }
}
*/

/*
Enter the first number: 5
Enter the second number: 2
Enter the third number: 3
Enter the fourth numbr: 6
product is: 180
 */

//---------------------------------------------------------------------------

/*
 Practical-5: If you have two integers stored in variables var1 an dvar2, 
what boolean test can you perform to see if on eor the other (but not both) 
is greater than 10?
 */

/*
using System;
namespace ConsoleApplication
{
 public class Practical
    {
        static void Main(string[] args)
        {
            int var1, var2;
            Console.Write("Enter number first: ");
            var1 = Convert.ToByte(Console.ReadLine());
            Console.Write("Enter number second: ");
            var2 = Convert.ToByte(Console.ReadLine());
            if((var1>10 && var2<=10) || (var2>10 && var1 <= 10))
            {
                Console.WriteLine("Boolean test succedded Both number are not > 10");
            }
            else
            {
                Console.WriteLine("Boolean test failed both number are >10 or <10");
            }
        }
    }
}

*/

/*
Enter number first: 45
Enter number second: 8
Boolean test succedded Both number are not > 10
 */

//---------------------------------------------------------------------------------
